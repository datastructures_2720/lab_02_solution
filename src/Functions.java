import java.util.ArrayList;
import java.util.List;

import interfaces.IFunctions;

/**
 * 
 * This is a practice class that implements the interface 'IFunction'.
 *
 */
public class Functions implements IFunctions {

	@Override
	public double getAverage(double[] arr) {

		double res = 0;
		for (int i = 0; i < arr.length; i++) {
			res += arr[i];
		}
		res /= arr.length;
		return res;
	}

	@Override
	public double getMax(List<Double> l) {

		Double max = l.get(0);

		for (int i = 0; i < l.size(); i++) {
			if (max < l.get(i)) {
				max = l.get(i);
			}
		}
		return max;
	}

	@Override
	public double getMax(double[][] matrix) {
		
		double tmp = matrix[0][0];
		for(int i = 0; i < matrix.length; i++){
			for(int j = 0; j < matrix.length; j++) {
				if(matrix[i][j] > tmp)
					tmp = matrix[i][j];
			}
		}
		return tmp;
	}
	
	
	@Override
	public double initializeList(List<Double> l, int size) {

		double i = 0.0;
		while (size-- > 0) {
			l.add(i++);
		}
		return l.get(l.size() - 1);
	}

	
	@Override
	public double[][] createMatrix(int n) {

		double[][] m = new double[n][n];
		double t = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				m[i][j] = t++;
			}
		}
		return m;
	}
	
	
	@Override
	public List<Integer> findAllUppdercases(String w) {
		
		List<Integer> indices = new ArrayList<Integer>();
		for(int i = 0; i < w.length(); i++) {
			if(Character.isUpperCase(w.charAt(i)))
				indices.add(i);
		}
		return indices;
	}
	
	

	@Override
	public List<Integer> findChars(char c, String w) {
		
		List<Integer> indices = new ArrayList<Integer>();
		for(int i = 0; i < w.length(); i++) {
			if(Character.toLowerCase(w.charAt(i)) == Character.toLowerCase(c))
				indices.add(i);
		}
		return indices;
	}
	
	
	
	
	@Override
	public String cleanUp(String w) {
		
		List<Integer> index_spaces = this.findAllUppdercases(w);
		
		String tmp = w;
		int j = 0;
		int begin = 0, mid = 0, end = 0;
		for(int i = 0; i < index_spaces.size(); i++) {
			mid = index_spaces.get(i) + j;
			end = tmp.length();
			tmp = tmp.substring(begin, mid) + " " + tmp.substring(mid, end);
			j++;
		}
 
		List<Integer> index_dots = this.findChars('.', tmp);
		mid = 0; end = 0; j = 0;
		for(int i = 0; i < index_dots.size(); i++) {
			mid = index_dots.get(i) + j;
			end = tmp.length();
			tmp = tmp.substring(begin, mid + 1) + "\n" + tmp.substring(mid + 1, end);
			j++;
		}
		return tmp;
	}

}
	


