import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * 
 * -> Class: Data Structures - 2720 Lab:
 * -> LAB: 02
 * -> Date: Friday 31 Aug, 2018
 * -> Subject: Java Revision [Solution]
 * -> Lab Web-page: [https://sites.google.com/view/azimahmadzadeh/teaching/data-structures-2720]
 * 
 * @author Azim Ahmadzadeh [https://grid.cs.gsu.edu/~aahmadzadeh1/]
 */

public class Main {

	/**
	 * This is a nice class with a lot of great information.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		Functions fun = new Functions();

		/* ---- Fun 1: Get Average ---- */
		// TASK:
		// 1. Create an array & initialize it with { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 },
		// 2. Call 'getAverage' function on this array,
		// 3. Print out the result. (Is you result correct?)

		double[] arr = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		double res = fun.getAverage(arr);
		System.out.println("Fun1: " + res);
		/* ---- END Fun 1 ---- */

		
		
		
		/* ---- Fun 2: Get Max ---- */
		// TASK:
		// 1. Create a list & initialize it with {9.0, 5.0, 12.0, 0.0, -1.0, 14.3, 22.0,
		// -31.0, 7.0, 16.0},
		// 2. Call 'getMax()' function,
		// 3. Print out the result. (Is you result correct?)

		List<Double> myList = new ArrayList<Double>();
		Collections.addAll(myList, 9.0, 5.0, 12.0, 0.0, -1.0, 14.3, 22.0, -31.0, 7.0, 16.0);

		res = fun.getMax(myList);
		System.out.println("Fun2: " + res);
		/* ---- END Fun 2 ---- */
		
		
		
		/* ---- Fun 3: Get Max ---- */
		// TASK:
		// 1. Create a 3 by 3 matrix with the following numbers: {{-3,-2,-1},{0,1,2},{3,4,5}},
		// 2. Call 'getMax()' function,
		// 3. Print out the result. (Is you result correct?)

		double[][] mat = new double[][] {{-3,-2,-1},{0,1,2},{3,4,5}};
		
		res = fun.getMax(mat);
		System.out.println("Fun3: " + res);
		/* ---- END Fun 3 ---- */
		
		

		
		
		/* ---- Experiment on Time ---- */
		// TASK:
		// 1. Run 'initializeList' for a 'LinkedList' and 1000 elements.
		// 2. Get the execution time needed for this task.
		// 3. Run 'initializeList' again, this time for an ArrayList and 1000 elements.
		// 4. Get the execution time again.
		// 5. Repeat this experiment several times while increasing 1000 to 100,000 and
		// 10,0000,000.
		//
		// What do you observe? Any explanation?
		//
		//	-----------------------------[MY OBSERVASION]----------------------------
		//	FOR LARGE ENOUGH LISTS, INITIALIZATION OF LINKEDLIST TAKES MORE
		//	TIME THAN FOR ARRAYLIST. I NOTICED CONSISTANT RESULTS FOR SIZE > 1000000
		//  -------------------------------------------------------------------------

		List<Double> list1 = new LinkedList<>();
		List<Double> list2 = new ArrayList<>();
		int size = 100;
		long startTime = 0, totalTime = 0;

		startTime = System.nanoTime();
		double size1 = fun.initializeList(list1, size);
		totalTime = System.nanoTime() - startTime;
		System.out.println("LinkedList: [" + size1 + "] elements added in [" + totalTime / 1000000 + "] miliseconds.");

		startTime = System.nanoTime();
		double size2 = fun.initializeList(list2, size);
		totalTime = System.nanoTime() - startTime;
		System.out.println("ArrayList: [" + size2 + "] elements added in [" + totalTime / 1000000 + "] miliseconds.");

		/* ---- END Experiment ---- */

		
		
		/* ---- Experiment on Memory ---- */
		// TASK:
		// 1. Run 'createMatrix' and print out the number of rows and colums,
		// 2. Repeat this while increasing the value of n (100, 1000, 10000)
		// 3. What is the smallest n for which you get an error?
		// 4. What kind of error you get? Why?
		//
		//	-----------------------------[MY OBSERVASION]----------------------------
		//	FOR CREATING MATRICES WITH MORE THAN 14000 ROWS AND 14000 COLUMNS, I GET
		//	"OUTOFMEMORY" REFERING TO THE JAVA HEAP SPACE.
		//  -------------------------------------------------------------------------

		double[][] m = fun.createMatrix(13000);
		System.out.println("N of Rows: " + m.length);
		System.out.println("N of Cols: " + m[0].length);

		/* ---- END Experiment ---- */
		
		
		
		/* ---- Fun 5: Clean up text ---- */
		// TASK:
		// 1. Create a string initialized with "ThisIsSoSuspicious.WeNeedToLearnJava.WeShould."
		// (exactly as it is typed here).
		// 2. Call 'cleanUp()' function, to separate the words with a single space, and to add
		// new line right after each dot.
		// 3. Print out the result. (Is you result correct?)

		String w = "ThisIsSoSuspicious.WeNeedToLearnJava.WeShould.";
//		List<Integer> indices = fun.findChars('s', "ThisIsSoSuspicious.WeNeedToLearnJava.WeShould");
		String result = fun.cleanUp(w);
		System.out.println(result);
		/* ---- END Fun 5 ---- */
	}

}
